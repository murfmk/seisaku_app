U:RDoc::TopLevel[	i I"README.rdoc:ETcRDoc::Parser::Simpleo:RDoc::Markup::Document:@parts[@S:RDoc::Markup::Heading:
leveli:	textI"Ruby OAuth;To:RDoc::Markup::BlankLine S;	;
i;I"Status;T@o:RDoc::Markup::Paragraph;[I"�{<img src="https://travis-ci.org/oauth-xx/oauth-ruby.svg?branch=master" alt="Build Status" />}[https://travis-ci.org/oauth-xx/oauth-ruby];T@@S;	;
i;I"	What;T@o;;[I"\This is a RubyGem for implementing both OAuth clients and servers in Ruby applications.;T@o;;[I"3See the OAuth specs http://oauth.net/core/1.0/;T@S;	;
i;I"Installing;T@o:RDoc::Markup::Verbatim;[I"sudo gem install oauth
;T:@format0o;;[I"dThe source code is now hosted on the OAuth GitHub Project http://github.com/oauth-xx/oauth-ruby;T@S;	;
i;I"The basics;T@o;;[I"�This is a ruby library which is intended to be used in creating Ruby Consumer and Service Provider applications. It is NOT a Rails plugin, but could easily be used for the foundation for such a Rails plugin.;T@o;;[I"�As a matter of fact it has been pulled out from an OAuth Rails GEM (https://rubygems.org/gems/oauth-plugin https://github.com/pelle/oauth-plugin) which now uses this gem as a dependency.;T@S;	;
i;I"Demonstration of usage;T@o;;[I"gWe need to specify the oauth_callback url explicitly, otherwise it defaults to "oob" (Out of Band);T@o;;[I"<@callback_url = "http://127.0.0.1:3000/oauth/callback"
;T;0o;;[I"GCreate a new consumer instance by passing it a configuration hash:;T@o;;[I"P@consumer = OAuth::Consumer.new("key","secret", :site => "https://agree2")
;T;0o;;[I",Start the process by requesting a token;T@o;;[
I"T@request_token = @consumer.get_request_token(:oauth_callback => @callback_url)
;TI"
;TI"+session[:token] = request_token.token
;TI"3session[:token_secret] = request_token.secret
;TI"Predirect_to @request_token.authorize_url(:oauth_callback => @callback_url)
;T;0o;;[I"-When user returns create an access_token;T@o;;[	I"Xhash = { oauth_token: session[:token], oauth_token_secret: session[:token_secret]}
;TI"Erequest_token  = OAuth::RequestToken.from_hash(@consumer, hash)
;TI"5@access_token = @request_token.get_access_token
;TI"0@photos = @access_token.get('/photos.xml')
;T;0o;;[I"oNow that you have an access token, you can use Typhoeus to interact with the OAuth provider if you choose.;T@o;;[I"require 'typhoeus'
;TI"4require 'oauth/request_proxy/typhoeus_request'
;TI"Joauth_params = {:consumer => oauth_consumer, :token => access_token}
;TI"!hydra = Typhoeus::Hydra.new
;TI"Zreq = Typhoeus::Request.new(uri, options) # :method needs to be specified in options
;TI"\oauth_helper = OAuth::Client::Helper.new(req, oauth_params.merge(:request_uri => uri))
;TI"`req.options[:headers].merge!({"Authorization" => oauth_helper.header}) # Signs the request
;TI"hydra.queue(req)
;TI"hydra.run
;TI"@response = req.response
;T;0S;	;
i;I"More Information;T@o:RDoc::Markup::List:
@type:BULLET:@items[o:RDoc::Markup::ListItem:@label0;[o;;[I"DRDoc: http://rdoc.info/github/oauth-xx/oauth-ruby/master/frames;To;;0;[o;;[I"IMailing List/Google Group: http://groups.google.com/group/oauth-ruby;T@S;	;
i;I"How to submit patches;T@o;;[I"dThe source code is now hosted on the OAuth GitHub Project http://github.com/oauth-xx/oauth-ruby;T@o;;[I"�To submit a patch, please fork the oauth project and create a patch with tests. Once you're happy with it send a pull request and post a message to the google group.;T@S;	;
i;I"License;T@o;;[I"AThis code is free to use under the terms of the MIT license.;T@S;	;
i;I"Contact;T@o;;[I"[OAuth Ruby has been created and maintained by a large number of talented individuals. ;TI"5The current maintainer is Aaron Quint (quirkey).;T@o;;[I"uComments are welcome. Send an email to via the OAuth Ruby mailing list http://groups.google.com/group/oauth-ruby;T:
@file@:0@omit_headings_from_table_of_contents_below0