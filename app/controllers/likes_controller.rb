class LikesController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy]
  before_action :set_post
  
  def create
    @like = Like.create(user_id: current_user.id, comment_id: params[:comment_id])
    @likes = Like.where(comment_id: params[:comment_id])
    @comment.reload
  end

  def destroy
    like = Like.find_by(user_id: current_user.id, comment_id: params[:comment_id])
    like.destroy
    @likes = Like.where(comment_id: params[:comment_id])
    @comment.reload
  end

  private

  def set_post
    @comment = Comment.find(params[:comment_id])
  end
end
