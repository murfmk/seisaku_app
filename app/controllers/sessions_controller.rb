class SessionsController < ApplicationController
  def new
  end

  def create
  @user = User.find_by(email: params[:session][:email])
    if @user && @user.authenticate(params[:session][:password])
      login @user
      params[:session][:remember_me] == '1' ? remember(@user) : forget(@user)
      redirect_back_or @user
    else
      flash.now[:danger] = 'メールアドレス、またはパスワードが正しくありません'
      render 'new'
    end
  end

  def twitter
      # request.env['omniauth.auth']にユーザのTwitter認証情報が格納されている
      user_data = request.env['omniauth.auth']
      session[:nickname] = user_data[:info][:nickname]
      redirect_to root_path, notice: 'ログインしました'
    end

  def destroy
    logout if logged_in?
    reset_session
    redirect_to root_url
  end
end
